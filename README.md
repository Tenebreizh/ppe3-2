#Sujet: Gestion des stocks de médicaments.

## *Cahier des charges :*

GSB est un fabriquant pharmaceutique, 
GSB aimerait faire la promotion de ces produits en les faisant parvenir aux médecins,
pour cela l'entreprise pharmaceutique a besoin d'un système de gestion des stocks de médicaments.


GSB a besoin :


* Une interface de suivi des stocks	
* De supprimer et d'ajouter des stocks
* De connaître les dates d'entrée en stock
* De gérer les stocks (système case)
* D'imposer un seuil de réapprovisionnement 
* De connaître les mouvements de stock (entrée/sortie)
* De connaître le stock du visiteur médical (seuil de réapprovisionnement)
* De lister des médicaments et leurs conditionnement 
* De supprimer et d'ajouter des médicaments
	
##Utilisateurs :
### *Gestionnaire :*
* Sa fonction est d'entrée et de sortir des médicaments.
* Il peut ajouter, supprimer ou modifier un médicament à la fois.
* Modifier la quantité et le libelle du médicament.

##Interface Gestionnaire :
* Afficher la liste des médicaments 
* Ajouter/supprimer/modifier les médicaments


##Base de données :
* Base de données Mysql/Oracle
* Table  medicaments, utilisateur


